package appolis.kurt.nwaim;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import appolis.kurt.nwaim.views.MainFragment;

public class Main extends ActionBarActivity {
	private FragmentTransaction transaction = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		android.support.v7.app.ActionBar bar = getSupportActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2f4447")));
		bar.setTitle(getResources().getString(R.string.main_title));
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			transaction = getSupportFragmentManager().beginTransaction();
			transaction.add(R.id.container, new MainFragment());

			transaction.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.action_write:
			Intent intent = new Intent(Main.this,
					appolis.kurt.nwaim.Contact.class);
			startActivity(intent);
			break;
		case R.id.action_settings:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

}
