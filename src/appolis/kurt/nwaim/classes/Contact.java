package appolis.kurt.nwaim.classes;

import android.graphics.Bitmap;



public class Contact {
	private String id, name, surname, status_message, profile= null;
	private  Bitmap profile_picture ;

	public Contact(String a, String b, String c, String d, Bitmap e, String id) {
		this.name = a;
		this.surname = b;
		this.status_message = c;
		this.profile = d;
		this.profile_picture = e;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getStatus_message() {
		return status_message;
	}

	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}

	public Bitmap getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(Bitmap profile_picture) {
		this.profile_picture = profile_picture;
	}
}
