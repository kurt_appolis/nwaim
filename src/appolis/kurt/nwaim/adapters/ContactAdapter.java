package appolis.kurt.nwaim.adapters;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import appolis.kurt.nwaim.R;
import appolis.kurt.nwaim.classes.Contact;

public class ContactAdapter extends ArrayAdapter<Contact> {
	private ArrayList<appolis.kurt.nwaim.classes.Contact> contacts = new ArrayList<appolis.kurt.nwaim.classes.Contact>();

	public ContactAdapter(View view,
			ArrayList<appolis.kurt.nwaim.classes.Contact> contacts) {
		super(view.getContext(), R.layout.contact_list_item, contacts);
		this.contacts = contacts;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View this_view = convertView;
		if (this_view == null) {
			this_view = LayoutInflater.from(getContext()).inflate(
					R.layout.contact_list_item, null);
		}
		appolis.kurt.nwaim.classes.Contact contact = contacts.get(position);

		TextView name = (TextView) this_view.findViewById(R.id.name_textview);
		name.setText(contact.getName());

		TextView status = (TextView) this_view
				.findViewById(R.id.status_textview);
		status.setText(contact.getStatus_message());

		TextView profile = (TextView) this_view
				.findViewById(R.id.profile_textview);
		profile.setText(contact.getProfile());

		ImageView profile_picture_imageview = (ImageView) this_view
				.findViewById(R.id.profile_picture_imageview);

		Bitmap profile_picture = contact.getProfile_picture();
		profile_picture_imageview.setImageBitmap(profile_picture);

		return this_view;
	}

}
