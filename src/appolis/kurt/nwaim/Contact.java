package appolis.kurt.nwaim;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class Contact extends Activity {
	private ArrayList<appolis.kurt.nwaim.classes.Contact> contacts = new ArrayList<appolis.kurt.nwaim.classes.Contact>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		android.app.ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2f4447")));
		bar.setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.contact_list);
		setUpListView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contact_list, menu);
		return true;
	}

	private void setUpListView() {
		setupArrayAdapters();

		ListView contact_list_listview = (ListView) findViewById(R.id.contact_list_listview);
		appolis.kurt.nwaim.adapters.ContactAdapter contactAdapter = new appolis.kurt.nwaim.adapters.ContactAdapter(
				contact_list_listview, contacts);
		contact_list_listview.setAdapter(contactAdapter);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			super.onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setupArrayAdapters() {

		Cursor cursor = getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);
		while (cursor.moveToNext()) {
			InputStream inputStream = ContactsContract.Contacts
					.openContactPhotoInputStream(
							getContentResolver(),
							ContentUris
									.withAppendedId(
											ContactsContract.Contacts.CONTENT_URI,
											new Long(
													cursor.getString(cursor
															.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID)))));

			Bitmap bm = BitmapFactory.decodeStream(inputStream);

			contacts.add(new appolis.kurt.nwaim.classes.Contact(
					cursor.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)),
					cursor.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_PRESENCE)),
					cursor.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)),
					"HOME",
					bm,
					cursor.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID))));

		}
	}

}
